const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const cors = require('cors');

const port = 8000;
app.use(express.json());
app.use(cors());

app.post('/encode', (req, res) => {
        let encoded = Vigenere.Cipher('password').crypt(req.body.message);
        res.send({encoded: encoded});
});


app.post('/decode', (req, res) => {
    let decode  = Vigenere.Decipher('password').crypt(req.body.message);
    res.send({decoded: decode});
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});